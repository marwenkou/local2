from django.conf.urls import re_path,url

from .consumers import ChatConsumer

websocket_urlpatterns = [
    url(r'^ws/chat/(?P<username>[^/]+)/', ChatConsumer),
]

