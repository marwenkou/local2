# from django.urls import path
#
# from .views import ChatMessageViewSet
#
# urlpatterns = [
#     path('', ChatMessageViewSet.index, name='index'),
#     path('<str:room_name>/', ChatMessageViewSet.room, name='room'),
# ]

from django.urls import path
from chat.views import ThreadView

urlpatterns = [
    path('<str:username>/', ThreadView.as_view())

]