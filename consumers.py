# from django.contrib.auth import get_user_model
# from asgiref.sync import async_to_sync
# from channels.generic.websocket import WebsocketConsumer
# import json
# from .models import Message
#
# User = get_user_model()
#
#
# class ChatConsumer(WebsocketConsumer):
#
#     def fetch_messages(self, data):
#         messages = Message.objects.order_by('-timestamp').all()[:10]
#         content = {
#             'command': 'messages',
#             'messages': self.messages_to_json(messages)
#         }
#         print(content)
#         self.send_message(content)
#
#     def new_message(self, data):
#         author = self.scope['user']
#         author_user = User.objects.filter(username=author)[0]
#         if data['message']:
#             message = Message.objects.create(
#                 author=author_user,
#                 content=data['message'])
#             content = {
#                 'command': 'new_message',
#                 'message': self.message_to_json(message)
#             }
#             return self.send_chat_message(content)
#
#     def messages_to_json(self, messages):
#         result = []
#         for message in messages:
#             result.append(self.message_to_json(message))
#         return result
#
#     def message_to_json(self, message):
#         return {
#             'author': message.author.username,
#             'content': message.content,
#             'timestamp': str(message.timestamp)
#         }
#
#     commands = {
#         'fetch_messages': fetch_messages,
#         'new_message': new_message
#     }
#
#     def connect(self):
#         self.room_name = self.scope['url_route']['kwargs']['room_name']
#         self.room_group_name = 'chat_%s' % self.room_name
#         async_to_sync(self.channel_layer.group_add)(
#             self.room_group_name,
#             self.channel_name
#         )
#         self.accept()
#
#     def disconnect(self, close_code):
#         async_to_sync(self.channel_layer.group_discard)(
#             self.room_group_name,
#             self.channel_name
#         )
#
#     def receive(self, text_data):
#         data = json.loads(text_data)
#         self.commands[data['command']](self, data)
#
#     def send_chat_message(self, message):
#         async_to_sync(self.channel_layer.group_send)(
#             self.room_group_name,
#             {
#                 'type': 'chat_message',
#                 'message': message
#             }
#         )
#
#     def send_message(self, message):
#         self.send(text_data=json.dumps(message))
#
#     def chat_message(self, event):
#         message = event['message']
#         self.send(text_data=json.dumps(message))


from channels.consumer import SyncConsumer
from asgiref.sync import async_to_sync
from django.contrib.auth.models import User
from chat.models import Thread, Message
import json

class ChatConsumer(SyncConsumer):

    def websocket_connect(self,event):
        self.u = self.scope['user']
        me = User.objects.get(id=1)
        other_username = self.scope['url_route']['kwargs']['username']
        other_user = User.objects.get(username=other_username)
        self.thread_obj = Thread.objects.get_or_create_personal_thread(me, other_user)
        self.room_name = f'personal_thread_{self.thread_obj.id}'
        async_to_sync(self.channel_layer.group_add)(self.room_name, self.channel_name)
        self.send({
            'type': 'websocket.accept'
        })
        print(f'[{self.channel_name}] - you are connected')


    def websocket_receive(self, event):
        print(f'[{self.channel_name}] - Received message - {event["text"]}')
        msg = json.dumps({
            'text': event.get('text'),
            'username':self.scope['user'].username

        })
        self.store_message(event.get('text'))

        async_to_sync(self.channel_layer.group_send)(
            self.room_name,
            {
                'type': 'websocket.message',
                'text': msg
            }
        )

    def websocket_message(self, event):
        print(f'[{self.channel_name}] - Message sent - {event["text"]}')
        self.send({
            'type': 'websocket.send',
            'text': event.get('text')
        })


    def websocket_disconnect(self,event):
        print(f'[{self.channel_name}] - Disconnected')
        async_to_sync(self.channel_layer.group_discard)(self.room_name, self.channel_name)
        print(event)

    def store_message(self, text):
        Message.objects.create(
            thread=self.thread_obj,
            sender=self.u,
            text=text
        )



###################################################################
class EchoConsumer(SyncConsumer):

    def websocket_connect(self,event):
        self.room_name = 'broadcast'
        self.send({
            'type' : 'websocket.accept'
        })
        async_to_sync(self.channel_layer.group_add)(self.room_name, self.channel_name)
        print(f'[{self.channel_name}] - you are connected')


    def websocket_receive(self, event):
        print(f'[{self.channel_name}] - received message - {event["text"]}')
        async_to_sync(self.channel_layer.group_send)(
            self.room_name,
            {
                'type': 'websocket.message',
                'text': event.get('text')
            }
        )

    def websocket_message(self, event):
        print(f'[{self.channel_name}] - Message sent - {event["text"]}')
        self.send({
            'type': 'websocket.send',
            'text': event.get('text')
        })


    def websocket_disconnect(self,event):
        print(f'[{self.channel_name}] - Disconnected')
        async_to_sync(self.channel_layer.group_discard)(self.room_name, self.channel_name)
        print(event)